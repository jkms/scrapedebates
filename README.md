# ScrapeDebates
This is a quick and dirty (emphasis on *dirty*) scraper for pulling dates and info from www.100debates.ca
## Instructions
```
git clone git@gitlab.com:jkms/scrapedebates.git
cd scrapedebates
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt 
python3 scrape.py 
```

