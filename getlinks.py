from bs4 import BeautifulSoup
import requests
baseurl = "www.100debates.ca/debates?page="
f = open("links.txt", "w+")
for i in range(1, 12):
    url = baseurl + str(i)
    r = requests.get("https://" + url)
    data = r.text
    soup = BeautifulSoup(data)
    for card in soup.find_all('div', attrs={'class': 'card event-card'}):
        for link in card.find_all('a'):
            f.write(link.get('href'))
            f.write("\r\n")
            print(link.get('href'))

f.close()

# for x in range(0, 3):
#    print("We're on time %d" % (x))
