from bs4 import BeautifulSoup, Comment
import re
import requests
import csv

baseurl = "www.100debates.ca"
number = 0

csvdata = [['URL', 'DATE', 'LOCATION', 'TITLE', 'NOTES']]
with open('links.txt') as topo_file:
    for line in topo_file:
        number += 1
        url = "https://" + baseurl + line.strip()
        print(str(number) + " " + url)
        r = requests.get(url)
        data = r.text
        soup = BeautifulSoup(data, "html.parser")
        [s.extract() for s in soup('script')]
        for event in soup.find_all('div', attrs={'class': "event-detail"}):
            p = event.find_all('p')
            eventinfo = [str(url)]
            for blurb in p:
                for element in blurb(text=lambda text: isinstance(text, Comment)):
                    element.extract()
                temp = str(''.join(lineitem.strip()
                            for lineitem in blurb.findAll(text=True)))
                temp = re.sub('\n', '', temp)
                eventinfo.append(temp)
            eventinfo[4:] = [''.join(eventinfo[4:])]
            #for n in range(len(eventinfo)):
                    #eventinfo[n] = re.sub('"', '""', eventinfo[n])
                    #eventinfo[n] = '"' + eventinfo[n] + '"'
            print(eventinfo)
            csvdata.append(eventinfo)
#csvdata = map(lambda s: s.strip(), csvdata)
with open('output.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerows(csvdata)
